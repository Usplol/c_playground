#include <stdlib.h>
#include <stdio.h>


int* twoSum(int* nums, int numsSize, int target)
{
    int *result = malloc(sizeof(int) * 2);

    for(int i = 0; i < numsSize; i++)
    {
        for(int j = i+1; j < numsSize; j++)
        {
            if ((nums[i] + nums[j]) == target)
            {
               result[0] = i;
               result[1] = j;
               return result;
            }
           
        }  

           
    }
    
     return result;
}

int main(int argc, char const *argv[])
{
    int nums[] = {2,7,11,15};

    int *result = twoSum(nums, sizeof(nums)/sizeof(int), 216);

    printf("[%d , %d]\n", result[0], result[1]);

    system("pause");
    return 0;
}

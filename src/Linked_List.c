#include <stdio.h>
#include <stdlib.h>

typedef struct device
{
	int address;
	struct device *next;
} device_t;

void release(device_t **ptr)
{
	free(*ptr);
	*ptr = NULL;
}

int main()
{

	int a;
	int c;
	device_t *ptr;

	device_t *head = NULL;
	device_t *last = NULL;
	device_t *current = NULL;

	device_t *device1 = NULL;
	device_t *device2 = NULL;
	device_t *device3 = NULL;

	device1 = (device_t *)malloc(sizeof(device_t));
	device2 = (device_t *)malloc(sizeof(device_t));
	device3 = (device_t *)malloc(sizeof(device_t));

	printf("Device 1 address: %X\n", device1);
	printf("Device 2 address: %X\n", device2);
	printf("Device 3 address: %X\n", device3);

	printf("sizeof device 1: %d\n", sizeof(device1));

	device1->address = 1;
	device2->address = 2;
	device3->address = 3;

	device1->next = device2;
	device2->next = device3;

	printf("sizeof device 1: %lu\n", sizeof(device_t));

	head = device1;
	last = device3;
	last->next = NULL;
	current = head;

	while (current != NULL)
	{
		printf("current device's memory address: %d\n", current);
		printf("current device's pointer's memory address: %d\n", current);

		current = current->next;
	}

	device1->next = NULL;
	release(&device2);

	system("pause");

	return 0;

	int d = 0;
}

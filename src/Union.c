#include <stdio.h>

union CpuMachine
{
    int i;
    char array[2];
} UnionCpu;

int main(int argc, char* argv[])
{
    UnionCpu.array[0] = 0x55;
    UnionCpu.array[1] = 0x33;

    printf("i 0x%x\n",UnionCpu.i);
	return 0;
}
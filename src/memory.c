
#include <stdio.h>
#include <math.h>
#include <stdlib.h>


int main(int argc, char const *argv[])
{
    /* code */
    printf("sizeof(int): %d\n",sizeof(int));
    
    int *data;
    int a[5] = {1,2,3,4,5};
    int *pa = (int *)&a;
    int *pb = &a[1];
    int *p = (int *)(&a + 1);

    
    data = calloc(2, sizeof(int));

    data[0] = 0;
    data[1] = 1;
    

    printf("size of data: %d\n", sizeof(data));
    printf("size of p: %d\n", sizeof(p));
    

    
    realloc(data, sizeof(data) * 2);

    data[1] = 1;
    data[2] = 2;
    data[3] = 3;

     printf("data[0]: %d\n", *(data + 0));
     printf("data[1]: %d\n", *(data + 1));
     printf("data[2]: %d\n", *(data + 2));
     printf("data[3]: %d\n", *(data + 3));
     printf("data[4]: %d\n", *(data + 4));

    system("pause");

    return 0;
}
